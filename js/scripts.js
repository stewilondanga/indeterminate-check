var navigate = (function() {
  $('.dd').toggle();
  $('.dd_btn').click(function() {
    var dataName = $(this).attr('data-name');
    $('.dd').hide();
    $('.' + dataName).toggle();
  });
})();

const leftCheck = document.getElementById('left-checkbox')
const rightCheck = document.getElementById('right-checkbox')
const leftSign = document.querySelector('.left')
const rightSign = document.querySelector('.right')
const isIndeterminate = false
let shouldGoLeft = false
let shouldGoRight = false
const checkIfIndeterminate = () => {

  if (shouldGoLeft && shouldGoRight) {
    leftCheck.indeterminate = true;
    rightCheck.indeterminate = true;
  } else {
    leftCheck.indeterminate = false;
    rightCheck.indeterminate = false;
  }
  console.log(leftCheck.indeterminate, rightCheck.indeterminate)
}
leftCheck.addEventListener('change', function() {
  shouldGoLeft = this.checked;
  if (this.checked) {
    leftSign.classList.add('go-left');
  } else {
    leftSign.classList.remove('go-left');
  }
  checkIfIndeterminate();
});

rightCheck.addEventListener('change', function() {
  shouldGoRight = this.checked;
  if (this.checked) {
    rightSign.classList.add('go-right');
  } else {
    rightSign.classList.remove('go-right');
  }
  checkIfIndeterminate();
});
